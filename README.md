**About**

This program uses a [Sharp GP2Y1010](http://www.sharp-world.com/products/device/lineup/data/pdf/datasheet/gp2y1010au_e.pdf) 
dust sensor as a [Homie Node](https://homieiot.github.io/) on an [ESP8266 D1 mini Lite](https://www.wemos.cc/en/latest/d1/d1_mini_lite.html) board.
It runs continuously, reporting the counts over [MQTT](https://mqtt.org/) at a configurable interval.

It works best with [Homie for ESP8266 / ESP32 v3](https://github.com/homieiot/homie-esp8266/tree/v3.0)

---

## Basic Operation

The GP2Y1010 sensor outputs an analog pulse with a pulse height proportional to the
particle size / density.

Due to a bug in the ESP8266 analog read function (WiFi becomes unstable if read too 
often), this program uses a digital GPIO input and calculates an approximate analog 
value by counting the number of loops that the sensor output is above the digital 
LOW/HIGH threshold.

It also computes a rough histogram, based on the number of counts per 10ms cycle.

The interval (in seconds) is set via the "interval" setting in the config.json

```json
{
    "name": "Dust Sensor",
    "device_id": "dust-1",
    "device_stats_interval": 300,
    "wifi": {
      "ssid": "<SSID>",
      "password": "<Password>"
    },
    "mqtt": {
      "host": "<MQTT broker IP>",
      "port": 1883,
      "base_topic": "<base topic>/"
    },
    "ota": {
      "enabled": true
    },
    "settings": {
      "interval" : 60
    }
}
```

The config.json must be installed to the ESP8266, e.g. using
[ESP8266FS-0.5.0](https://github.com/esp8266/arduino-esp8266fs-plugin/releases/download/0.5.0/ESP8266FS-0.5.0.zip)
to upload the configuration to the ESP to /homie/config.json, 
[as documented here](https://arduino-esp8266.readthedocs.io/en/2.7.4_a/filesystem.html#uploading-files-to-file-system)

See the documentation on the [JSON configuration file](https://homieiot.github.io/homie-esp8266/docs/3.0.1/configuration/json-configuration-file/).

## Compilation

This project is intended for the Arduino IDE.

Clone this repository and load the "Homiev3_DustSensor_Sharp" project into the Arduino IDE.


You will need to manually install the Homie [dependencies](https://homieiot.github.io/homie-esp8266/docs/3.0.1/quickstart/getting-started/).

Examples that worked for me are:

1. [ArduinoJson >= 5.0.8](https://github.com/bblanchon/ArduinoJson/archive/v6.17.2.zip) 
1. [Bounce2](https://github.com/thomasfredericks/Bounce2/archive/v2.53.zip)
1. [ESPAsyncTCP >= c8ed544](https://codeload.github.com/me-no-dev/ESPAsyncTCP/zip/15476867dcbab906c0f1d47a7f63cdde223abeab)
1. [async-mqtt-client-0.8.1](https://github.com/marvinroger/async-mqtt-client/archive/v0.8.1.zip)
1. [ESPAsyncWebServer](https://codeload.github.com/me-no-dev/ESPAsyncWebServer/zip/f6fff3f91ebf45b91ca4cff2460d2febd61f1e27)

## Wiring Diagram

While the SHARP GPY2Y1010 is a 5V device, I used the 3.3V supply to power the photo-diode sensor to guarantee
that the sensor output (Vo) will not exceed the maximum rated voltage of the ESP8266 GPIO line.

| SHARP GPY2Y1010 | Weimos D1 Mini | Signal  |
| -------------   |:-------------:| -----:|
| 1. V-LED        | 5V 			| LED power |
| 2. LED-GND      | Gnd      	| LED ground  |
| 3. LED-Enable   | D2      	| LED enable  |
| 4. S-GND        | Gnd      	| Sensor ground  |
| 5. Vo           | D5      	| Sensor output  |
| 6. Vcc          | 3V3      	| Sensor power   |

### External Components

Note that the SHARP GPY2Y1010 uses two external components to help with pulse shaping. 

A 150Ω resistor and 220 μF capacitor are required, as shown in figure 1, taken from the [Sharp GP2Y1010](http://www.sharp-world.com/products/device/lineup/data/pdf/datasheet/gp2y1010au_e.pdf) datasheet. 

![Figure 1](/GPY2Y1010AU0F_Figure1.png)

You can see how I've mounted them in my (rather hacky) installation, below.

![Actual Hardware](/Actual_Hardware.jpg)


## The Data

The data is output on MQTT topics as follows:

1. <topic_prefix>/<device_id>/particle-size/summary
1. <topic_prefix>/<device_id>/particle-size/status
1. <topic_prefix>/<device_id>/particle-size/unit
1. <topic_prefix>/<device_id>/particle-size/value
1. <topic_prefix>/<device_id>/particle-size/histo

Where "topic_prefix" and "device_id" are specified in the JSON configuration file.

The *value* topic is produced at the interval specified in the JSON configuration.

The *histo* topic is only produced when value is greater than 0 and contains a 10-bin histogram of the pseudo analog signal:
```
1, 0, 1, 4, 0, 0, 0, 0, 0, 0
0, 0, 1, 1, 3, 9, 2, 0, 0, 0
0, 0, 2, 0, 2, 0, 0, 0, 0, 0
0, 0, 0, 1, 1, 2, 6, 0, 0, 0
```

## OTA
[Over-the-Air (OTA) updates](https://homieiot.github.io/homie-esp8266/docs/3.0.1/others/ota-configuration-updates/) is 
enabled and supported using the included [scripts](https://github.com/homieiot/homie-esp8266/tree/v3.0/scripts/ota_updater).

In Arduino.cc, use CTRL-ALT-S to export the binary to the project directory.

To upload the binary, use a command similar to the following:
```bash
$ ota_updater.py -l <your.mqtt.server> -t "<topic_prefix>/" -i <device_id> <name_of_binary.bin>

```
where:

- *<your.mqtt.server>* is the name or IP address of your MQTT server,
- *<topic_prefix>* is your topic prefix,
- *<device_id>* is the ID of the device that you wish to upgrade, and
- *<name_of_binary.bin>* identifies the binary you wish to upload.

## TODOs
For future work:

1. Make it sleep for X minutes, sample for Y seconds, then report.
