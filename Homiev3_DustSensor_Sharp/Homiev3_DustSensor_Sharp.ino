/**
   This program uses a Sharp GP2Y1010 dust sensor as a Homie Node on an ESP8266 board.
   It runs continuously, reporting the counts over MQTT at a configurable interval.

   The GP2Y1010 sensor outputs an analog pulse with a pulse height proportional to the
   particle size / density.

   Due to a bug in the ESP8266 analog read function (WiFi becomes unstable if read too 
   often, this program uses a digital GPIO input and calculates an approximate analog 
   value by counting the number of loops that the sensor output is above the digital 
   LOW/HIGH threshold.

   It also computes a rough histogram, based on the number of counts per 10ms cycle.

   The interval (in seconds) is set via the "interval" setting in the config.json:

  {
    "name": "Dust Sensor",
    "device_id": "dust-1",
    "device_stats_interval": 300,
    "wifi": {
      "ssid": "<SSID>",
      "password": "<Password>"
    },
    "mqtt": {
      "host": "<MQTT broker IP>",
      "port": 1883,
      "base_topic": "<base topic>/"
    },
    "ota": {
      "enabled": true
    },
    "settings": {
      "interval" : 60
    }
  }

   TODO: Make it sleep for X minutes, sample for Y seconds, then report.

  Using Homie convention v3.0.1
    https://github.com/homieiot/homie-esp8266/archive/v3.0.1.zip
    https://homieiot.github.io/specification/
    https://homieiot.github.io/homie-esp8266/docs/3.0.0/others/cpp-api-reference/


  Board:
    WRL-13678: WeMOS D1 R2 & Mini, 4M Flash, 2MB FS (~1019kB OTA)
    
  URLs:
    https://www.aliexpress.com/item/32830838323.html
    https://github.com/IOT-MCU

   OTA Update:
    ota_updater.py -l nassy.lan -t "home/" -i ESP8266-dust-1 Homiev3_DustSensor_Sharp.ino.d1_mini-v1.0.3.bin

   SPIFSS update:
    python /home/kevin/.arduino15/packages/esp8266/hardware/esp8266/2.6.3/tools/upload.py write_flash 0x200000 Homiev3_DustSensor_Sharp.spiffs.bin

  History:
    v2.0.0 : Renaming topics: <topic_prefix>/<device_id>/dust -> <topic_prefix>/<device_id>/particle-size
    v1.0.3 : Added "status" topic
    v1.0.2 : Added "value" topic (same as "counts") - all my sensors provide a "value" topic, to support wildcard subscriptions.
    v1.0.1 : ??
    v1.0.0 : Original   

 **/

#include <Homie.h>

// NB: You must change the Arduino Board setting to match the hardware
#define HW_DESC "WeMOS D1 R2 Mini, ESP8266 4M"

#define FW_NAME "dust-wemos-sharp"
#define FW_VERSION "2.0.0"

/* Magic sequence for Autodetectable Binary Upload */
const char *__FLAGGED_FW_NAME = "\xbf\x84\xe4\x13\x54" FW_NAME "\x93\x44\x6b\xa7\x75";
const char *__FLAGGED_FW_VERSION = "\x6a\x3f\x3e\x0e\xe1" FW_VERSION "\xb0\x30\x48\xd4\x1a";



void onHomieEvent(const HomieEvent& event) {
  if  (event.type == HomieEventType::WIFI_CONNECTED) {
    Serial << " Event -> HomieEventType::WIFI_CONNECTED" << endl;
    Serial << "Wi-Fi connected, IP: " << event.ip << ", gateway: " << event.gateway << ", mask: " << event.mask << endl;

    const HomieInternals::ConfigStruct& config = Homie.getConfiguration();
    Serial << " Configuration name: " << config.name << endl;
    Serial << " Configuration deviceId: " << config.deviceId << endl;

  } else if (event.type == HomieEventType::MQTT_READY) {
    Serial << "MQTT connected" << endl;
  } else if (event.type == HomieEventType::MQTT_DISCONNECTED) {
    Serial << "MQTT disconnected, reason: " << (int8_t)event.mqttReason << endl;
    delay(500);
  }
}

// ------------------------------------------ DustSensor stuff -------------------
const int SENSOR_TRIGGER_PIN = 4; // Use GPIO4 as trigger
const int PARTICLE_PIN = 14;      // Use GPIO14 as sensor

#define PULSE_INTERVAL 10         // milliseconds between pulses
#define PULSE_INTERVAL_COUNT 500  // How many loops to integrate pulse counter? Manually configured.
#define SENSOR_LED_ON LOW
#define SENSOR_LED_OFF HIGH
unsigned long nextPulseTimeMs = 0;
unsigned long dust_counts = 0;

#define HISTO_BINS 10
unsigned int histogram[HISTO_BINS];

// Reporting interval
unsigned long messageInterval = 60000; // Default: Once per minute
unsigned long nextMessageTimeMs = 0;

HomieNode dustCounterNode("particle-size", "SHARP GP2Y1010 Dust particle counter" , "sensor");
HomieSetting<long> delaySetting("interval", "Reporting interval (s)");  // id, description


void setup() {
  Serial.begin(115200);
  Serial << endl << endl;

  pinMode(SENSOR_TRIGGER_PIN, OUTPUT);
  pinMode(PARTICLE_PIN, INPUT);

  // Set Homie LED pin to GPIO2
  Homie.setLedPin(2, LOW);
  Homie_setFirmware(FW_NAME, FW_VERSION); // The underscore is not a typo! See Magic bytes

  // Populate the node name with default + setting
  delaySetting.setDefaultValue(60);

  Homie.onEvent(onHomieEvent);
  Homie.setSetupFunction(setupHandler);//.setLoopFunction(loopHandler);
  dustCounterNode.advertise("summary");
  dustCounterNode.advertise("hardware");
  dustCounterNode.advertise("status");
  dustCounterNode.advertise("unit");
  dustCounterNode.advertise("count");
  dustCounterNode.advertise("value");
  dustCounterNode.advertise("histo");

  Homie.setup();

  const HomieInternals::ConfigStruct& config = Homie.getConfiguration();
  Homie.getLogger() << "SHARP GP2Y1010 particle counter implementation v" << FW_VERSION << endl;
  Homie.getLogger() << "MQTT topic root: " << config.mqtt.baseTopic << config.deviceId << endl;
  Homie.getLogger() << "Reporting interval set to " << delaySetting.get() << " seconds" << endl;
  messageInterval = delaySetting.get() * 1000;
}

void setupHandler() {
  dustCounterNode.setProperty("unit").send("#");

  char summary[100];
  snprintf(summary, sizeof(summary), "Integrating for %ld seconds", delaySetting.get());
  dustCounterNode.setProperty("summary").send(summary);
  dustCounterNode.setProperty("hardware").send(HW_DESC);
  dustCounterNode.setProperty("status").send("Initialising");  
}

// histogram functions...
// Update the histogram, but only if there are any counts at all.
void updateHistogram(unsigned int tempCounts) {
  if (tempCounts > 0) {
    unsigned int histo_interval = PULSE_INTERVAL_COUNT / HISTO_BINS; // 500/10 -> min of 50
    unsigned int index = tempCounts / histo_interval;
    // Some sanity checks
    if (index >= HISTO_BINS) {
      index = HISTO_BINS - 1;
    }
    histogram[index]++;
  }
}

// Convert the histogram into a string
String histoToString() {
  String result = "";
  for (int i = 0; i < HISTO_BINS - 1; i++) {
    result += String(histogram[i]) + ", ";
  }
  result += String(histogram[HISTO_BINS - 1]);

  return result;
}

void histoReset() {
  for (int i = 0; i < HISTO_BINS - 1; i++) {
    histogram[i] = 0;
  }
}
// ... histogram functions

// Our sensor's loop
//WARNING: Only use this if the ".setLoopFunction(loopHandler);" is not used!
void loopHandler() {
  unsigned long now = millis();

  // TODO: Do this properly. The code below will produce spam on every tick until the rollover occurs.
  // Check for roll-over of time
  if ((now + PULSE_INTERVAL) < nextPulseTimeMs) {
    dustCounterNode.setProperty("status").send("Roll-over detected");
    Homie.getLogger() << "millis() roll-over detected. Resetting timers." << " counts" << endl;
    nextPulseTimeMs = now + PULSE_INTERVAL;
    nextMessageTimeMs = now + messageInterval;
    return;
  }

  // Handle waveform: High
  if (now >= nextPulseTimeMs) {
    unsigned long tempCounts = 0;
    digitalWrite(SENSOR_TRIGGER_PIN, SENSOR_LED_ON);
    nextPulseTimeMs = now + PULSE_INTERVAL;

    // Brute loop: 450 loops ~ 0.280ms
    for (int i = 0; i < PULSE_INTERVAL_COUNT; i++) {
      tempCounts += digitalRead(PARTICLE_PIN);  // IO0
    }
    digitalWrite(SENSOR_TRIGGER_PIN, SENSOR_LED_OFF);

    updateHistogram(tempCounts);

    // Simple, just aggregrate
    dust_counts += (tempCounts / (PULSE_INTERVAL_COUNT / HISTO_BINS));
  }

  if (now >= nextMessageTimeMs) {
    nextMessageTimeMs = now + messageInterval;

    if (Homie.isConnected()) {
      String value(dust_counts);
      dustCounterNode.setProperty("count").send(value);
      dustCounterNode.setProperty("value").send(value);
      dustCounterNode.setProperty("status").send("OK");
      if (dust_counts > 0) {
        dustCounterNode.setProperty("histo").send(histoToString());
      }
    }

    // Report the statistics over the serial port even if not connected to the MQTT broker
    Homie.getLogger() << "Dust counts: " << dust_counts << " counts" << endl;
    if (dust_counts > 0) {
      Homie.getLogger() << "Dust histo: " << histoToString() << endl;
      histoReset();
      dust_counts = 0;
    }
  }
}

void loop() {
  Homie.loop();
  //WARNING: Only use this if the ".setLoopFunction(loopHandler);" is not used!
  // Running loopHandler here allows reporting over the serial port
  // even if the broker is not connected.
  loopHandler();
}
